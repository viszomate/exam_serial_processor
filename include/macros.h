/*
 * macros.h
 *
 *  Created on: Apr 18, 2020
 *      Author: zamek
 */

#ifndef MACROS_H_
#define MACROS_H_

#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>

#define MALLOC(ptr,size) \
	do { \
		ptr=malloc(size); \
		if (!ptr) {		\
			syslog(LOG_ERR, "malloc result is NULL"); \
			exit(-1);	\
		} \
	} while(0);

#define FREE(ptr) \
		do {  \
			free(ptr); \
			ptr=NULL; \
		} while(0)

#endif /* MACROS_H_ */
