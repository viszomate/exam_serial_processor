/*
 * serial.h
 *
 *  Created on: Apr 18, 2020
 *      Author: zamek
 */

#ifndef SERIALQUEUE_H_
#define SERIALQUEUE_H_

#include <sys/queue.h>
/**
 * Maximum lenght of data, you can change it.
 */
#define MAX_DATA_LENGTH 50

/**
 * \enum commands_t
 */
enum commands_t {
	/// The command is unknown
	CMD_UNKNOWN=0, 
	/// The command is start
	CMD_START=1, 
	/// The command is stop
	CMD_STOP=2, 
	/// The command is to get the data
	CMD_GET_DATA=0x10, 
	/// The command is reset
	CMD_RESET=0xf
};
/**
 * \struct serial_packet_t
 */
typedef struct serial_packet_t {
	/// state of the packet
	int valid;
	/// sender address of packet
	char from_address;
	/// arrival adress of packet
	char to_address;
	/// command type of the packet
	char command;
	/// data lenght of the packet
	short data_length;
	/// maximal data lenght of the packet
	char data[MAX_DATA_LENGTH];
	TAILQ_ENTRY(serial_packet_t) next;
} serial_packet_t;

/**
 * \brief init serial module
 * 
 * If you forget to call this there will be some unexpected results
 *
 * \return return with EXIT_SUCCESS if successfully or EXIT_FAILURE if something went wrong, prints to log
 * in both ways
 */
int serial_init(int queue_size);

/**
 * \brief clear and free serial packet list
 *
 * \return return EXIT_SUCCESS if everything alright or EXIT_FAILURE if something went wrong, prints to log
 * in both ways
 */
int serial_deinit();

int serial_add_packet(const char from_address,const  char to_address,const char command,const short data_length, char *data);

/**
 * \brief serial queue size
 *
 * \return updates queue_size to serial_queue
 */
int serial_queue_size();

/**
 * \brief serial queue size
 *
 * \return updates unused_queue_size to serial_queue
 */
int serial_unused_queue_size();

/**
 * \brief serial get packet
 *
 * \return return NULL if serial_queue.initialized or serial_queue.queue_size is 0
 */
struct serial_packet_t *serial_get_packet();

/**
 * \brief serial give back processed packet
 *
 * \return return EXIT_FAILURE if packet is NULL
 */
int serial_give_back_processed_packet(struct serial_packet_t *packet);



#endif /* SERIALQUEUE_H_ */
