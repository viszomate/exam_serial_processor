#Makefile for serial

APP=serial

DEBUG=yes
SRC=src
BIN=bin
INC=include

CFLAGS= -pedantic -std=c11 -D_DEFAULT_SOURCE

ifeq ($(DEBUG),yes) 
CFLAGS+=-g
endif

LIBS=-l cunit -l pthread

OBJS=$(patsubst $(SRC)/%.c,$(BIN)/%.o, $(wildcard $(SRC)/*.c)) 

$(BIN)/$(APP): $(BIN) $(OBJS)
		$(CC) -o $(BIN)/$(APP) $(OBJS) $(LIBS) $(CFLAGS)
		
$(BIN):
		mkdir $(BIN)
		
$(BIN)/%.o:$(SRC)/%.c
		$(CC) -o $@ -c $< -I $(INC) $(CFLAGS)

clean:
		$(RM) -r $(BIN)

all: clean $(BIN)/$(APP)

test: all
		$(BIN)/$(APP)
		
.PHONY: clean all

		
				
