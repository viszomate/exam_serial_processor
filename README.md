# Feladatmegoldás: exam_serial_processor #
Készítette: - Csomor Péter
            - Viszocsánszki Máté

# Feladatmegoldás menete # 

1. Kód értelmezése, áttekintése
2. A test alapján visszafejtés (hibák javítása, hiányosságok pótlása)
3. Makefile megírása
4. Debugging
5. Test


# Test eredmény #

 CUnit - A unit testing framework for C - Version 2.1-3
     http://cunit.sourceforge.net/


Suite: Suite 1
  Test: Serial Packet test ...passed

Run Summary:    Type  Total    Ran Passed Failed Inactive
              suites      1      1    n/a      0        0
               tests      1      1      1      0        0
             asserts     90     90     90      0      n/a

Elapsed time =    0.001 seconds
