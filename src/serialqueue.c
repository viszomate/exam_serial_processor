/*
 * serial.c
 *
 *  Created on: Apr 18, 2020
 *      Author: zamek
 */

#include <stdlib.h>
#include <stdarg.h>
#include <strings.h>
#include <string.h>
#include <macros.h>
#include "serialqueue.h"
#include <pthread.h>
#include <syslog.h>

#define ERR_QUEUE_IS_NOT_INITIALIZED "Queue is not initailized yet"
#define ERR_QUEUE_IS_ALREADY_INITIALIZED "Queue is already initialized"
#define ERR_MUTEX_INIT_FAILED "Mutex init failed"
#define ERR_MUTEX_LOCKING_FAILED "Locking mutex failed"
#define ERR_MUTEX_UN_LOCKING_FAILED "Unlocking mutex failed"
#define ERR_MUTEX_DESTROY "Mutex destroy failed"
#define ERR_UNKNOWN_COMMAND "Unknown command %d"
#define ERR_QUEUE_SIZE_IS_LESS_OR_EQUAL_ZERO "queue size is less or equal zero"
#define ERR_CANNOT_CREATE_EMPTY_PACKET_FOR_UNUSED "cannot create empty packet for unused queue"
#define ERR_DATA_LENGTH_IS_ZERO_BUT_DATA_IS_NOT_NULL "data length is zero but data is not null"
#define ERR_DATA_LENGTH_IS_NOT_ZERO_BUT_DATA_IS_NULL "data length is not zero but data is null"


#define DEBUG

static struct {
	TAILQ_HEAD(unused_pool, serial_packet_t) unused;
	TAILQ_HEAD(used_pool, serial_packet_t) used;
	int queue_size;
	int unused_queue_size;
    int initialized;
    pthread_mutex_t lock;
} serial_queue = {
		.queue_size = 0,
		.initialized = 0,
		.unused_queue_size = 0,
		.lock = PTHREAD_MUTEX_INITIALIZER
};

static struct serial_packet_t *create_empty_packet() {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter create_empty_packet");
#endif
	if (!serial_queue.initialized) {
		syslog(LOG_ERR, ERR_QUEUE_IS_NOT_INITIALIZED);
		return NULL;
	}

	serial_packet_t *new;
	MALLOC(new, sizeof(serial_packet_t));
	return new;
}

static void serial_free_packet(struct serial_packet_t *p) {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter serail_free_packet, p is null:%s", p?"false":"true");
#endif

	FREE(p);
}

int serial_init(int queue_size){
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter serial_init, queue_size:%d", queue_size);
#endif
	if (queue_size <= 0) {
		syslog(LOG_ERR, ERR_QUEUE_SIZE_IS_LESS_OR_EQUAL_ZERO);
		return EXIT_FAILURE;
	}

	if (serial_queue.initialized) {
		syslog(LOG_ERR, ERR_QUEUE_IS_ALREADY_INITIALIZED);
		return EXIT_FAILURE;
	}

#ifndef NO_THREAD
	if (pthread_mutex_init(&serial_queue.lock, NULL)) {
		syslog(LOG_ERR, ERR_MUTEX_INIT_FAILED);
		return EXIT_FAILURE;
	}
#endif

	TAILQ_INIT(&serial_queue.unused);
	TAILQ_INIT(&serial_queue.used);
	serial_queue.initialized = 1;

	for (int i = 0; i < queue_size; ++i) {
		struct serial_packet_t *p = create_empty_packet();
		if (!p) {
			syslog(LOG_ERR, ERR_CANNOT_CREATE_EMPTY_PACKET_FOR_UNUSED);
			return EXIT_FAILURE;
		}
		TAILQ_INSERT_TAIL(&serial_queue.unused, p, next);
	}

	serial_queue.unused_queue_size = queue_size;

	return EXIT_SUCCESS;
}

int serial_deinit(){
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter serial_deinit");
#endif

	if (!serial_queue.initialized) {
		syslog(LOG_ERR, ERR_QUEUE_IS_NOT_INITIALIZED);
		return EXIT_FAILURE;
	}

	struct serial_packet_t *p;
	while ((p=serial_get_packet()))
		serial_free_packet(p);

	while((p=TAILQ_FIRST(&serial_queue.unused))) {
		TAILQ_REMOVE(&serial_queue.unused, p, next);
		serial_free_packet(p);
	}

	if (pthread_mutex_destroy(&serial_queue.lock)) {
		syslog(LOG_ERR, ERR_MUTEX_DESTROY);
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

static int check_command(char command) {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter check_command, command:%d",command);
#endif
	switch(command) {
	case CMD_GET_DATA:
	case CMD_RESET:
	case CMD_START :
	case CMD_STOP :  return 1;
	default : {
		syslog(LOG_WARNING, ERR_UNKNOWN_COMMAND, command);
		return 0;
		}
	}
}

int serial_add_packet(const char from_address, const char to_address, const char command, const short data_length, char *data){
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter serial_add_packet, from:%d, to:%d, command:%d, data_length:%d, data is null:%s",
					from_address, to_address, command, data_length, data?"false":"true");
#endif
	if (!serial_queue.initialized) {
		return EXIT_FAILURE;
	}

	if (serial_queue.unused_queue_size == 0) {
		return EXIT_FAILURE;
	}

#ifndef NO_THREAD
	pthread_mutex_lock(&serial_queue.lock);
#endif
	serial_packet_t *new_packet = serial_queue.unused.tqh_first;

	new_packet->from_address = from_address;
	new_packet->to_address = to_address;
	new_packet->command = command;
	new_packet->data_length = data_length;
	for (int i = 0; i < data_length; i++) {
		new_packet->data[i] = data[i];
	}
	new_packet->valid = 1;

	TAILQ_REMOVE(&serial_queue.unused, serial_queue.unused.tqh_first, next);
	TAILQ_INSERT_TAIL(&serial_queue.used, new_packet, next);
	serial_queue.unused_queue_size--;
	serial_queue.queue_size++;
#ifndef NO_THREAD
	pthread_mutex_unlock(&serial_queue.lock);
#endif

	return EXIT_SUCCESS;
}

int serial_queue_size() {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter serial_queue_size");
#endif
	return serial_queue.queue_size;

}

int serial_unused_queue_size() {
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter serial_unused_queue_size");
#endif
	return serial_queue.unused_queue_size;
}

struct serial_packet_t *serial_get_packet(){
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter serial_get_packet");
#endif

	if (!serial_queue.initialized || serial_queue.queue_size == 0) {
		return NULL;
	}

#ifndef NO_THREAD
	pthread_mutex_lock(&serial_queue.lock);
#endif
	serial_packet_t *first = TAILQ_FIRST(&serial_queue.used);
	serial_queue.queue_size--;
	TAILQ_REMOVE(&serial_queue.used, TAILQ_FIRST(&serial_queue.used), next);
#ifndef NO_THREAD
	pthread_mutex_unlock(&serial_queue.lock);
#endif
	return first;
}

int serial_give_back_processed_packet(struct serial_packet_t *packet){
#ifdef DEBUG
	syslog(LOG_DEBUG, "Enter serial_give_back_processed_packet");
#endif

	if (packet == NULL) {
		return EXIT_FAILURE;
	}

#ifndef NO_THREAD
	pthread_mutex_lock(&serial_queue.lock);
#endif
	packet->valid = 0;
	TAILQ_INSERT_TAIL(&serial_queue.unused, packet, next);
	serial_queue.unused_queue_size++;
#ifndef NO_THREAD
	pthread_mutex_unlock(&serial_queue.lock);
#endif

	return EXIT_SUCCESS;
}
