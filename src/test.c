/*
 * test.c
 *
 *  Created on: Apr 18, 2020
 *      Author: zamek
 */

#include <stdlib.h>
#include <CUnit/Basic.h>
#include <serialqueue.h>
#include <pthread.h>
#include <syslog.h>

const int QUEUE_SIZE=10;

char FROM_ADDRESS = 'a';
char TO_ADDRESS = 'b';
char COMMAND = 'q';
char data[] = {'a','b','c','d','e','f'};

pthread_t producer;
pthread_t consumer;

#define NO_THREAD

int init_test() {
	return 0;
}

int close_test() {
	return  0;
}

void *producer_thread(void *param) {
	syslog(LOG_DEBUG, "Enter Producer Thread");
	for (int i=0;i<QUEUE_SIZE; ++i) {
		CU_ASSERT_EQUAL(EXIT_SUCCESS, serial_add_packet(i+1, i+1, CMD_RESET, 0, NULL));
	}
	return NULL;
}

void *consumer_thread(void *param) {
	syslog(LOG_DEBUG, "Enter Consumer Thread");
	struct serial_packet_t *p =NULL;
	for (int i=0;i<QUEUE_SIZE; ++i) {
		p = serial_get_packet();
		CU_ASSERT_PTR_NOT_NULL(p);
		CU_ASSERT_EQUAL(i+1, p->from_address);
		CU_ASSERT_EQUAL(i+1, p->to_address);
		CU_ASSERT_EQUAL(CMD_RESET, p->command);
		CU_ASSERT_EQUAL(0, p->data_length);
		CU_ASSERT_EQUAL(1, p->valid);
		CU_ASSERT_EQUAL(EXIT_SUCCESS, serial_give_back_processed_packet(p));
	}
	return NULL;
}

void packet_test() {
	syslog(LOG_DEBUG, "try to add packet before initialized");
	CU_ASSERT_EQUAL(EXIT_FAILURE, serial_add_packet(FROM_ADDRESS, TO_ADDRESS, COMMAND, sizeof(data), data));
	syslog(LOG_DEBUG, "try to get packet size before initialized");
	CU_ASSERT_EQUAL(0, serial_queue_size());
	syslog(LOG_DEBUG, "try to get unused packet size before initialized");
	CU_ASSERT_EQUAL(0, serial_unused_queue_size());
	syslog(LOG_DEBUG, "try to get a packet before initialized");
	CU_ASSERT_PTR_NULL(serial_get_packet());
	syslog(LOG_DEBUG, "try to give back a packet before initialized");
	CU_ASSERT_EQUAL(EXIT_FAILURE, serial_give_back_processed_packet(NULL));

	syslog(LOG_DEBUG, "init queue");
	CU_ASSERT_EQUAL(EXIT_SUCCESS, serial_init(QUEUE_SIZE));

#ifdef NO_THREAD
	syslog(LOG_DEBUG, "NO THREAD TEST");
	producer_thread(NULL);
	consumer_thread(NULL);
#else
	syslog(LOG_DEBUG, "THREAD TEST");
	pthread_create(&producer, NULL, producer_thread, NULL);
	pthread_create(&consumer, NULL, consumer_thread, NULL);
	pthread_join(producer, NULL);
	pthread_join(consumer, NULL);
#endif
	syslog(LOG_DEBUG, "after test, queue size must be 0");
	CU_ASSERT_EQUAL(0, serial_queue_size());
	syslog(LOG_DEBUG, "after test, unused queue size must be %d", QUEUE_SIZE);
	CU_ASSERT_EQUAL(QUEUE_SIZE, serial_unused_queue_size());

	syslog(LOG_DEBUG, "after test, getpacket must return with NULL");
	struct serial_packet_t * p=serial_get_packet();
	CU_ASSERT_PTR_NULL(p);
	CU_PASS("Packet test finished");
}

int main() {
	openlog("SERIAL TEST", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
	setlogmask (LOG_UPTO (LOG_DEBUG));

	CU_pSuite suite =NULL;
	if (CUE_SUCCESS!=CU_initialize_registry())
		goto exit;

	suite = CU_add_suite("Suite 1", init_test, close_test);
	if (NULL==suite) {
		CU_cleanup_registry();
		goto exit;
	}

	if (NULL==CU_add_test(suite, "Serial Packet test", packet_test)) {
		CU_cleanup_registry();
		goto exit;
	}

	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();

exit:
	closelog();
	return CU_get_error();
}
